-- Lua 5.4 constants. Mostly from lopcodes.h

local Constants = {}

Constants.LUA_SIGNATURE = {27, 76, 117, 97} -- in ascii: \x1bLua = <ESC>Lua
Constants.Instruction = 4
Constants.INT_MAX =  0xFFFFFFFF
Constants.UINT_MAX = 0xFFFFFFFF
Constants.SIZET_MAX = (1 << 8 * string.packsize("J")) - 1
Constants.version = 504
Constants.format = 0
Constants.LUAC_DATA = {25, 147, 13, 10, 26, 10} -- in ascii: \x19\x93\r\n\x1a\n
Constants.luac_int = 22136
Constants.luac_num = 370.5


Constants.opnames = {
	[0] = "MOVE",
	"LOADI",
	"LOADF",
	"LOADK",
	"LOADKX",
	"LOADBOOL",
	"LOADNIL",
	"GETUPVAL",
	"SETUPVAL",
	"GETTABUP",
	"GETTABLE", -- 10
	"GETI",
	"GETFIELD",
	"SETTABUP",
	"SETTABLE",
	"SETI",
	"SETFIELD",
	"NEWTABLE",
	"SELF",
	"ADDI",
	"ADDK", -- 20
	"SUBK",
	"MULK",
	"MODK",
	"POWK",
	"DIVK",
	"IDIVK",
	"BANDK",
	"BORK",
	"BXORK",
	"SHRI", -- 30
	"SHLI",
	"ADD",
	"SUB",
	"MUL",
	"MOD",
	"POW",
	"DIV",
	"IDIV",
	"BAND",
	"BOR", -- 40
	"BXOR",
	"SHL",
	"SHR",
	"MMBIN",
	"MMBINI",
	"MMBINK",
	"UNM",
	"BNOT",
	"NOT",
	"LEN", -- 50
	"CONCAT",
	"CLOSE",
	"TBC",
	"JMP",
	"EQ",
	"LT",
	"LE",
	"EQK",
	"EQI",
	"LTI", -- 60
	"LEI",
	"GTI",
	"GEI",
	"TEST",
	"TESTSET",
	"CALL",
	"TAILCALL",
	"RETURN",
	"RETURN0",
	"RETURN1", -- 70
	"FORLOOP",
	"FORPREP",
	"TFORPREP",
	"TFORCALL",
	"TFORLOOP",
	"SETLIST",
	"CLOSURE",
	"VARARG",
	"VARARGPREP",
	"EXTRAARG"
}



Constants.opcodes = {}
for k, v in pairs(Constants.opnames) do
	Constants.opcodes[v] = k
end
function Constants.getstr(x)
	assert(type(x)=='table','tried to getstr of '..type(x))
	local r = ""
	for _, v in ipairs(x) do
		r = r .. string.char(v)
	end
	return r
end
function Constants.UPVALNAME(f, x)
	return ((f.upvalues[x].name) and Constants.getstr(f.upvalues[x].name) or "-")
end

Constants.luaT_eventname = {
	[0] = "__index",
	"__newindex",
	"__gc",
	"__mode",
	"__len",
	"__eq",
	"__add",
	"__sub",
	"__mul",
	"__mod",
	"__pow",
	"__div",
	"__idiv",
	"__band",
	"__bor",
	"__bxor",
	"__shl",
	"__shr",
	"__unm",
	"__bnot",
	"__lt",
	"__le",
	"__concat",
	"__call",
	"__close"
}
Constants.luaT_eventnameR = {}
for k, v in pairs(Constants.luaT_eventname) do
	Constants.luaT_eventnameR[v] = k
end


local function MASK1(n, p)
    return ((~((0xFFFFFFFF) << (n))) << (p)) & 0xFFFFFFFF
end


local SIZE_C = 8
local SIZE_B = 8
local SIZE_Bx = (SIZE_C + SIZE_B + 1)
local SIZE_A = 8
local SIZE_Ax = (SIZE_Bx + SIZE_A)
local SIZE_sJ = (SIZE_Bx + SIZE_A)

local SIZE_OP = 7
local POS_OP = 0

local POS_A = (POS_OP + SIZE_OP)
local POS_k = (POS_A + SIZE_A)
local POS_B = (POS_k + 1)
local POS_C = (POS_B + SIZE_B)

local POS_Bx = POS_k
local POS_Ax = POS_A
local POS_sJ = POS_A

--#if L_INTHASBITS(SIZE_sJ)
local MAXARG_sJ = ((1 << SIZE_sJ) - 1)
---#else
--#define MAXARG_sJ	MAX_INT
--#endif

local OFFSET_sJ = (MAXARG_sJ >> 1)

local function L_INTHASBITS(b)
    return ((Constants.UINT_MAX >> ((b) - 1)) >= 1)
end
local MAXARG_B =((1<<SIZE_B)-1)
local MAXARG_Bx
if L_INTHASBITS(SIZE_Bx) then
    MAXARG_Bx = ((1 << SIZE_Bx) - 1)
else
    MAXARG_Bx = Constants.INT_MAX
end
local MAXARG_C = ((1 << SIZE_C) - 1)
local OFFSET_sBx = (MAXARG_Bx >> 1)
local OFFSET_sC = (MAXARG_C >> 1)

local cast_int = function(i)
    return i
end
local getarg = function(i, pos, size)
    return (cast_int(((i) >> (pos)) & MASK1(size, 0)))
end
local sC2int = function(i)
    return ((i) - OFFSET_sC)
end
local GET = {}
GET.OPCODE = function(i)
    return (i >> POS_OP) & MASK1(SIZE_OP, 0)
end
GET.ARG_A = function(i)
    return getarg(i, POS_A, SIZE_A)
end
GET.ARG_B = function(i)
    return getarg(i, POS_B, SIZE_B)
end
GET.ARG_C = function(i)
    return getarg(i, POS_C, SIZE_C)
end
GET.ARG_Ax = function(i)
    return getarg(i, POS_Ax, SIZE_Ax)
end
GET.ARG_Bx = function(i)
    return getarg(i, POS_Bx, SIZE_Bx)
end
GET.ARG_sBx = function(i)
    return getarg(i, POS_Bx, SIZE_Bx) - OFFSET_sBx
end
GET.ARG_k = function(i)
    return getarg(i, POS_k, 1)
end

GET.ARG_sB = function(i)
    return sC2int(GET.ARG_B(i))
end
GET.ARG_sC = function(i)
    return sC2int(GET.ARG_C(i))
end
GET.ARG_sJ = function(i)
    return getarg(i, POS_sJ, SIZE_sJ) - OFFSET_sJ
end
function Constants.GetInstruction(instruction)
    local instr = {
      opcode = GET.OPCODE(instruction),
      opname = Constants.opnames[GET.OPCODE(instruction)],
      A = GET.ARG_A(instruction),
      B = GET.ARG_B(instruction),
      C = GET.ARG_C(instruction),
      Ax = GET.ARG_Ax(instruction),
      Bx = GET.ARG_Bx(instruction),
      sB = GET.ARG_sB(instruction),
      sC = GET.ARG_sC(instruction),
      sBx = GET.ARG_sBx(instruction),
	  isk = GET.ARG_k(instruction),
	  sJ = GET.ARG_sJ(instruction)
	}
    return instr
end
Constants.GET = GET

function Constants.eventname(x)
    return Constants.luaT_eventname[x]
end

Constants.offset = {
	A	   = POS_A,
	K	   = POS_k,
	B	   = POS_B,
	C	   = POS_C,
	Bx     = POS_Bx,
	sBx    = POS_Bx,
	Ax     = POS_Ax,
	sJ     = POS_sJ,
	opcode = POS_OP,
	isk    = POS_k,
	sC	   = POS_C,
	sB	   = POS_B,
	
}
Constants.signed_offset = {
	sBx = MAXARG_Bx >> 1,
	sJ  = MAXARG_sJ >> 1,
	sB  = MAXARG_B >> 1,
	sC  = MAXARG_C >> 1
}

Constants.header = {
	data = { 25, 147, 13, 10, 26, 10, 14 },
	lua_inst_size = 4,
	lua_int = 8,
	lua_num = 8,
	luac_data = { 25, 147, 13, 10, 26, 10 },
	luac_format = 0,
	luac_int = 22136,
	luac_num = 370.5,
	ok = true,
	s1 = 27,
	s2 = 76,
	s3 = 117,
	s4 = 97,
	version = 504
}
Constants.Soffset = 0xFFFA

return Constants