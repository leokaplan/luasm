-- dumps an IR to bytecode, it is mostly a translation of ldump.c

local Constants = require("5_4.constants")
local luasm = require "src.luasm"
local Utils = require "lib.utils"

local Dump = {}

local DIBS = math.ceil(string.packsize('T') * 8 / 7)


function Dump.Vector(v, n, D)
	local res = {}
	for i = 1, n do
		if v[i] then
			table.insert(res, string.pack('B', v[i]))
		end
	end
	res = table.concat(res)
	D.writer(res)
end

function Dump.Size(x, D)
	local buff, n = {}, -1
	repeat
	   n = n + 1
	   -- x's first seven bits, the rest to zero
	   buff[DIBS - n] = x & 127;
	   x = x >> 7
	until x == 0 ;
	buff[DIBS] = buff[DIBS] | 128 ; -- mark last byte
	Dump.Vector(buff, DIBS, D)
end

-- local function dumpVar(v, D)
-- 	Dump.Vector(v, 1, D)
-- end

function Dump.Literal(s, D)
	--local literal = string.sub(string.pack('z', s), 1, -2)
	local literal = string.pack('c'..#s, s)
	D.writer(literal)
end

function Dump.Byte(y, D)
    local byte = string.pack('B', y)
	D.writer(byte)
end

function Dump.Integer(x, D)
	local int = string.pack('j', x)
	D.writer(int)
end

function Dump.Int(x, D)
	assert(x)
	Dump.Size(x, D)
end

function Dump.Number(x, D)
	local num = string.pack('n', x)
	D.writer(num)
end

--TODO get from code
function Dump.Header(D)
	Dump.Literal(Constants.getstr(Constants.LUA_SIGNATURE), D)
	Dump.Int(Constants.version, D)
	Dump.Byte(Constants.format, D)
	Dump.Literal(Constants.getstr(Constants.LUAC_DATA), D)
	Dump.Byte(Constants.Instruction, D)
	Dump.Byte(string.packsize('j'), D)
	Dump.Byte(string.packsize('n'), D)
	Dump.Integer(Constants.luac_int, D)
	Dump.Number(Constants.luac_num, D)
end

function Dump.String(str, D)
	local size = string.len(str)
	if size == 0 then
	    Dump.Size(0, D)
	else
		Dump.Size(size + 1, D)
		Dump.Literal(str, D)
	end
end


-- convert a command table into a value (an integer), 
local function to_value(instruction) 
    local opcode = Constants.opcodes[instruction.opname]
    local args = instruction[2]
    local value = 0
    -- encode opcode
	value = value | (opcode << Constants.offset['opcode'])
	-- 1 is dump type, 2 is parse/print type
	local dump_types = luasm.types[instruction.opname][1]
	for k, arg_type in ipairs(dump_types) do
		local arg = instruction[arg_type]
        local x
        local offset = Constants.offset[arg_type]
		if arg == nil then
			x = 0
		elseif type(arg) == 'table' then
            x = arg[2] 
        else
            x = arg
		end
        if not Utils.check_int(x) then
            error(instruction.opname.."["..k.."] = "..tostring(x).." ("..pi(arg)..") isnt integer")
        end
        -- check if the type is signed
		if string.sub(arg_type, 1, 1) == 's' then
            x = x + Constants.signed_offset[arg_type]
        end
        assert(offset,arg_type)
		if arg~=nil then 
			x = x << offset
			value = value | x 
		end
	end
	return value
end



function Dump.Code(instructions, D)
	local sizecode = # instructions
	Dump.Int(sizecode, D)
	for _, instruction in ipairs(instructions) do
		local value = to_value(instruction)
        D.writer(string.pack('I4',value))
	end
end

local CONSTANT_TYPE = {
	["integer"] = 35,
	["float"] = 19,
	["string"] = 20,
	["boolean"] = 1,
	["nil"] = 0,
}

function Dump.Constants(f, D)
	local constants = f.k
	local n = # constants
	Dump.Int(n, D)
    for _, constant in ipairs(constants) do
		if type(constant) == 'table' then
			if #constant == 1 and constant[1] == false then
				constant = nil
			else
				constant = Constants.getstr(constant)
			end
        end
        local t = type(constant)
		if t == "number" then 
			-- maybe (Constant.INT_MAX-1)/2
			if  constant == math.floor(constant) 
			and constant < Constants.INT_MAX
			and constant > -Constants.INT_MAX then 
				t = "integer"
			else 
				t = "float"
			end
		end
		Dump.Byte(CONSTANT_TYPE[t], D)
		if t == "nil" then 
			--error('nil constant')
		elseif t == "boolean" then 
			Dump.Byte(constant, D)
		elseif t == "string" then 
			Dump.String(constant, D)
		elseif t == "integer" then 
			Dump.Integer(constant, D) --complemento a 2
		elseif t == "float" then 
			Dump.Number(constant, D)
		else
			error("Can't dump constant of type "..t)
		end
	end
end

function Dump.Upvalues(f, D)
	local upvalues = f.upvalues
	local n = # upvalues
	Dump.Int(n, D)
	for _, upvalue in ipairs(upvalues) do
		Dump.Byte(upvalue.instack, D)
		Dump.Byte(upvalue.idx, D)
		Dump.Byte(upvalue.kind, D)
	end
end


function Dump.Debug(f, D)
	if not D.strip then
		Dump.Int(#f.debug.lineinfo, D)
		Dump.Vector(f.debug.lineinfo,#f.debug.lineinfo,D)
		Dump.Int(#f.debug.abslineinfo, D)
		for _, y in ipairs(f.debug.abslineinfo) do
			Dump.Int(y.pc, D)
			Dump.Int(y.line, D)
		end
		Dump.Int(#f.debug.locvars, D)
		for _, locvar in ipairs(f.debug.locvars) do
			Dump.String(Constants.getstr(locvar.varname), D)
			Dump.Int(locvar.startpc, D)
			Dump.Int(locvar.endpc, D)
		end
		Dump.Int(#f.upvalues, D)
		for _, upvalue in ipairs(f.upvalues) do
			Dump.String(Constants.getstr(upvalue.name), D)
		end
	else
		Dump.Int(0, D)
		Dump.Int(0, D)
		Dump.Int(0, D)
		Dump.Int(0, D)
	end
end


function Dump.Function(f, D,parent_source)
    if D.strip then
        Dump.String("", D)
    else
        if parent_source then
            if Constants.getstr(f.source) ~= Constants.getstr(parent_source) then
                Dump.String(Constants.getstr(f.source), D)
            else
                Dump.String("", D)
            end
		else
			if f.source then
				Dump.String(Constants.getstr(f.source), D)
			end
        end
	end
	local func = parent_source and f or f
    Dump.Int(func.linedefined, D)
    Dump.Int(func.lastlinedefined, D)
	Dump.Byte(func.num_params, D)
	Dump.Byte(func.is_vararg, D)
    Dump.Byte(func.maxstacksize, D)
    Dump.Code(func.code, D)
	Dump.Constants(func, D)
    Dump.Upvalues(func, D)
    Dump.Protos(func, D)
    Dump.Debug(func, D)

end

function Dump.Protos(f, D)
	local protos = f.p
	local n = # protos
    Dump.Int(n, D)
	for _, proto in ipairs(protos) do
        Dump.Function(proto, D,f.source)
	end
end
function Dump.Body(opcode,D)
    Dump.Byte(#opcode.upvalues, D)
    Dump.Function(opcode,D)
end
function Dump.dump(opcode,strip)
    local result = {}
    local D = {strip = strip, writer = function (str) assert(type(str)~='table',type(str));table.insert(result, str) end}
    Dump.Header(D)
    Dump.Body(opcode,D)

    return table.concat(result)
end

return Dump