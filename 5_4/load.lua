-- loads an bytecode into an IR, 
-- it is mostly a translation of lundump.c

local Constants = require("5_4.constants")
local luasm = require "src.luasm"
local Utils = require "lib.utils"


local Load = {}

function Load.Unsigned(text, limit, pos)
	local x = 0
	local b
	limit = limit >> 7
	repeat
		b, pos = string.unpack("B", text, pos)
		if math.ult(limit, x) then
			error("integer overflow")
		end
		x = (x << 7) | (b & 0x7f)
	until not ((b & 0x80) == 0)
	return x, pos
end

function Load.Size(text, pos)
	return Load.Unsigned(text, Constants.SIZET_MAX, pos)
end

function Load.StringN(text, pos)
	local size
	size, pos = Load.Size(text, pos)
	if size == 0 then
		return 0, {}, pos
	end
	size = size - 1
	local data = {string.unpack(string.rep("B", size), text, pos)}
	return size, Utils.remove_last(data)
end

function Load.Int(text, pos)
	return Load.Unsigned(text, Constants.INT_MAX, pos)
end

function Load.Byte(text, pos)
	return string.unpack("B", text, pos)
end
function Load.Vector(text, pos, n)
	return Utils.remove_last({string.unpack(string.rep("B", n), text, pos)})
end
function Load.String(text, pos)
	local size, string
	size, string, pos = Load.StringN(text, pos)
	if #string ~= size then
		error("bad format for constant string")
	end
	return string, pos
end

function Load.Integer(text, pos)
	return string.unpack("j", text, pos)
end
function Load.Number(text, pos)
	return string.unpack("n", text, pos)
end

function Load.Code(text, pos)
	local n
	n, pos = Load.Int(text, pos)
	local data = {string.unpack(string.rep("I4", n), text, pos)}
	return n, Utils.remove_last(data)
end
function Load.Constants(text, pos, f)
	local n
	n, pos = Load.Int(text, pos)
	f.k = {}
	--luaM_newvectorchecked(S.L, n, TValue);
	f.sizek = n
	local LUA_TNIL = 0
	local LUA_TBOOLEAN = 1
	local LUA_TNUMBER = 3
	local LUA_TSTRING = 4
	local LUA_TNUMFLT = (LUA_TNUMBER | (1 << 4))
	local LUA_TNUMINT = (LUA_TNUMBER | (2 << 4))
	local LUA_TSHRSTR = (LUA_TSTRING | (1 << 4))
	local LUA_TLNGSTR = (LUA_TSTRING | (2 << 4))
	--for i = 1, n do
	--		f.k[i] = nil
	--end
	for i = 1, n do
		local t
		t, pos = Load.Byte(text, pos)
		if t == LUA_TNIL then
			f.k[i] = {false}
		elseif t == LUA_TBOOLEAN then
			f.k[i], pos = Load.Byte(text, pos)
		elseif t == LUA_TNUMFLT then
			f.k[i], pos = Load.Number(text, pos)
		elseif t == LUA_TNUMINT then
			f.k[i], pos = Load.Integer(text, pos)
			--f.k[i] = f.k[i] * -1 -- complemento a 2
		elseif t == LUA_TLNGSTR or t == LUA_TSHRSTR then
			f.k[i], pos = Load.String(text, pos)
		else
			error("type error Loading constants")
		end
	end
	return pos
end

function Load.Upvalues(text, pos, f)
	local n
	n, pos = Load.Int(text, pos)
	f.upvalues = {}
	f.sizeupvalues = n
	for i = 1, n do
		f.upvalues[i] = {}
		f.upvalues[i].name = false
		f.upvalues[i].instack, pos = Load.Byte(text, pos)
		f.upvalues[i].idx, pos = Load.Byte(text, pos)
		f.upvalues[i].kind, pos = Load.Byte(text, pos)
	end
	return pos
end

function Load.Protos(text, pos, f)
	local n
	n, pos = Load.Int(text, pos)
	f.p = {}
	f.sizep = n
	for i = 1, n do
		f.p[i] = false
	end
	for i = 1, n do
		f.p[i] = {}
		f.p[i], pos = Load.Function(text, pos, f.p[i], f.source)
		--f.p[i].body.parent = f
		f.p[i].index = i
	end
	return pos
end

function Load.Debug(text, pos, f)
	local n, _
	f.debug = {}
	n, pos = Load.Int(text, pos)
	f.debug.lineinfo = {}
	f.debug.sizelineinfo = n
	f.debug.lineinfo, pos = Load.Vector(text, pos, n)
	n, pos = Load.Int(text, pos)
	f.debug.abslineinfo = {}
	f.debug.sizeabslineinfo = n
	for it = 1, n do
		f.debug.abslineinfo[it] = {}
		f.debug.abslineinfo[it].pc, pos = Load.Int(text, pos)
		f.debug.abslineinfo[it].line, pos = Load.Int(text, pos)
	end
	n, pos = Load.Int(text, pos)
	f.debug.locvars = {}
	f.debug.sizelocvars = n
	for it = 1, n do
		f.debug.locvars[it] = {}
		f.debug.locvars[it].varname = false
	end
	for it = 1, n do
		_, f.debug.locvars[it].varname, pos = Load.StringN(text, pos)
		f.debug.locvars[it].startpc, pos = Load.Int(text, pos)
		f.debug.locvars[it].endpc, pos = Load.Int(text, pos)
	end
	n, pos = Load.Int(text, pos)
	for it = 1, n do
		f.upvalues[it] = f.upvalues[it] or {}
		_, f.upvalues[it].name, pos = Load.StringN(text, pos)
	end
	return pos
end

function Load.Function(text, pos, f, psource)
	local _
	_, f.source, pos = Load.StringN(text, pos)
	if not f.source or Constants.getstr(f.source) == "" then -- no source in dump?
		--reuse parent's source
		f.source = Utils.copy(psource)
	end
	--assert(f.source,"without source (maybe stripped?)")
	f.linedefined, pos = Load.Int(text, pos)
	f.lastlinedefined, pos = Load.Int(text, pos)
	f.num_params, pos = Load.Byte(text, pos)
	f.is_vararg, pos = Load.Byte(text, pos)
	f.maxstacksize, pos = Load.Byte(text, pos)

	f.sizecode, f.code, pos = Load.Code(text, pos)
	for k, v in ipairs(f.code) do
		-- get instruction with all values
		local full = Constants.GetInstruction(v)
		-- get the correspondent dump type (ABC for instance)
		local dump_types = Utils.copy(luasm.types[full.opname][1])
		-- we also want opname
		table.insert(dump_types,"opname")
		-- and isk
		if luasm.isABC(full.opname) then 
			table.insert(dump_types,"isk")
		end
		-- keep only relevant
		local low = Utils.filter(full,dump_types)
		-- filter values that are not taken to luasm ( type = _ )
		local high_types = luasm.types[full.opname][2]
		for ki,typ in ipairs(high_types) do 
			if typ == '_' then
				low[dump_types[ki]] = nil
			end
		end
		f.code[k] = low
	end
	pos = Load.Constants(text, pos, f)
	pos = Load.Upvalues(text, pos, f)
	pos = Load.Protos(text, pos, f)
	pos = Load.Debug(text, pos, f)
	return f, pos
end

function Load.Header(x)
	local pos = 0
	local header = {}
	header.ok = true
	header.s1, header.s2, header.s3, header.s4, pos = string.unpack("bbbb", x, pos)
	header.ok = header.ok and Utils.cmp_ok({header.s1, header.s2, header.s3, header.s4}, Constants.LUA_SIGNATURE)
	header.version, pos = Load.Int(x, pos)
	header.ok = header.ok and header.version == Constants.version
	header.luac_format, pos = string.unpack("B", x, pos)
	header.ok = header.ok and header.luac_format == Constants.format
	header.data = {string.unpack(string.rep("B", 6), x, pos)}
	header.luac_data, pos = Utils.remove_last(header.data)
	header.ok = header.ok and Utils.cmp_ok(header.luac_data, Constants.LUAC_DATA)
	header.lua_inst_size, header.lua_int, header.lua_num, pos = string.unpack("BBB", x, pos)
	header.ok =
		header.ok and
		Utils.cmp_ok(
			{header.lua_inst_size, header.lua_int, header.lua_num},
			{Constants.Instruction, string.packsize("j"), string.packsize("n")},
			{"instruction", "int", "num"}
		)
	header.luac_int, pos = Load.Integer(x, pos)
	header.luac_num, pos = Load.Number(x, pos)
	header.ok = header.ok and Utils.cmp_ok({header.luac_int, header.luac_num}, {Constants.luac_int, Constants.luac_num})
	return header, pos
end
function Load.Body(text, pos)
	local f = {}
	local cl
	cl, pos = Load.Byte(text, pos)
	Load.Function(text, pos, f, nil)
	return f, pos
end
function Load.load(bytecode)
	local ret = {}
	local pos
	local header, pos = Load.Header(bytecode)
	assert(header.ok,"corrupted header")
	ret = Load.Body(bytecode, pos)
	ret.header = header
	return ret
end
return Load
