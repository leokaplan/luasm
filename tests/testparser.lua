local parseopcode = require "parseOpcode"
local inspect = require('inspect')
pp = function(...) return print(inspect(...)) end
ppi = function(...) return print(inspect(...,{newline=''})) end
local function read_file(path)
	local file = io.open(path, "rb")
	if not file then return nil end
	local content = file:read "*all"
	file:close()
	return content
end
local params = {...}
local filepath = params[1]
local content = read_file(filepath)
local parsed = parseopcode.parse(content)
