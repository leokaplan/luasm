local inspect = require('inspect')
pp = function(...) return print(inspect(...)) end
ppi = function(...) return print(inspect(...,{newline=''})) end
pi = function(...) return inspect(...,{newline=''}) end

local Constants = require "5_4.constants"
local load = require "5_4.load"
local dump = require "5_4.dump"
--local convert = require "5_4.convert"

--local luasm = require "src.luasm"
local luasmprint = require "src.print"
local parse = require "src.parser"

local argparse = require "argparse"
local parser = argparse("luasm", "")
parser:argument("input", "A Lua 5.4 module file.")
local args = parser:parse()



local function diff(str1,str2)
    local places = {}
    for i = 1,string.len(str1) do --Loop over strings
        if str1:sub(i,i) ~= str2:sub(i,i) then --If that character is not equal to it's counterpart
            table.insert(places,i)
        else
            print(i,string.format("%02x ",string.byte(str1:sub(i,i))),'_ok_')
        end
    end
    return places --Return the index after where the shorter one ends as fallback.
end
local function printdiff(str1,str2)
    local r = diff(str1,str2)
    for k,v in pairs(r) do
        local s1 = string.format("%02x ",string.byte(str1:sub(v,v)))
        local s2 = string.format("%02x ",string.byte(str2:sub(v,v)))
        print(v,s1,s2)
    end
end
local function printvals(k,v1,v2)
    if k == 'value' then
        local t1 = Constants.GetInstruction(v1)
        local t2 = Constants.GetInstruction(v2)
        print('different values',k)
        print(Constants.opnames[t1.opcode])
        ppi(t1)
        print(Constants.opnames[t2.opcode])
        ppi(t2)
    else
        print('different values',k,v1,v2)
    end
end
local function diffT(t1,t2,k)--,ignore_mt)
   k = k or "root"
   local ty1 = type(t1)
   local ty2 = type(t2)
   if ty1 ~= ty2 then return false,print('different types ',k,ty1,ty2) end
   -- non-table types can be directly compared
   if ty1 == 'number' and ty2 == 'number' then return math.abs(t1-t2)<0.0000000000001 end
   if ty1 ~= 'table' and ty2 ~= 'table' then return t1 == t2 end
   -- as well as tables which have the metamethod __eq
   --local mt = getmetatable(t1)
   --if not ignore_mt and mt and mt.__eq then return t1 == t2 end
   for k1,v1 in pairs(t1) do
      local v2 = t2[k1]
      if v2 == nil or not diffT(v1,v2,k1) then return false,printvals(k1,v1,v2) end
   end
   for k2,v2 in pairs(t2) do
      local v1 = t1[k2]
      if v1 == nil or not diffT(v1,v2,k2) then return false,printvals(k2,v1,v2) end
   end
   return true
end








local strip = false
local bytecode54 = string.dump(loadfile(args.input),strip)
local opcode54 = load.load(bytecode54)
--convert.clean(opcode54)
pp(opcode54)
-- local load_errors = convert.validate(opcode54)
-- if #load_errors > 0 then
--     print('bytecode loaded')
--     pp(opcode54)
--     for _,v in ipairs(load_errors) do
--         print(v)
--     end
--     error('LOAD FAILED')
-- end
print('LOAD OK')

local genbytecode54 = dump.dump(opcode54,strip)
local gen_opcode54 = load.load(genbytecode54)
pp(gen_opcode54)
diffT(opcode54,gen_opcode54)
print('DUMP OK')
--assert(type(bytecode54)==type(genbytecode54),'types dont match')
assert(string.len(bytecode54)==string.len(genbytecode54),
'sizes dont match: '..string.len(bytecode54)..' '..string.len(genbytecode54))

--printdiff(bytecode54,genbytecode54)
local function string2hex (str)
	local function tohex(c)
	  return string.format("%02x ",string.byte(c))
	end
	local hex = string.gsub(str, ".", tohex)
	return hex
end
local function split(str)
    local ret = {}
    for substring in str:gmatch("%S+") do
       table.insert(ret, substring)
    end
    return ret
end
print(string2hex(bytecode54))
print()
print(string2hex(genbytecode54))
local spcode54 = split(string2hex(bytecode54))
local genspcode54 = split(string2hex(genbytecode54))
diffT(spcode54,genspcode54)
assert(bytecode54==genbytecode54,'dump not working')
--print(pp(opcode54))
print('LOAD/DUMP OK')

-- local luassemblyir = convert.from(opcode54)
-- local errors = luasm.validate(luassemblyir)
-- if #errors > 0 then
--     print('luasm loaded')
--     pp(luassemblyir)
--     for _,v in ipairs(errors) do
--         print(v)
--     end
--     error("FROM FAILED")
-- end
-- print('FROM OK')
-- --pp(opcode54)
-- local genopcode54 = convert.to(luassemblyir)
-- convert.clean(genopcode54)
-- local load_errors_gen = convert.validate(genopcode54)
-- if #load_errors_gen > 0 then
--     print('bytecode loaded')
--     pp(genopcode54)
--     for _,v in ipairs(load_errors_gen) do
--         print(v)
--     end
-- end
-- print('TO OK')
-- print(pp(luassemblyir))
-- pp(opcode54)
-- pp(genopcode54)
-- --TODO remove
-- opcode54.body.source = {}
-- opcode54.body.debug.lineinfo = {}
-- opcode54.body.debug.locvars = {}
-- assert(diffT(opcode54,genopcode54))
-- print('FROM/TO OK')

--assert(opcode54==genopcode54)
local stringluassembly = luasmprint.print(opcode54)
print(stringluassembly)
print('PRINT OK')
--print(stringluassembly)
local genluassemblyir = parse.parse(stringluassembly)
--assert(opcode54==gen2opcode54)
-- local errors2 = luasm.validate(genluassemblyir)
-- if #errors2 > 0 then
--     print('luasm gen')
--     pp(genluassemblyir)
--     for _,v in ipairs(errors2) do
--         print(v)
--     end
-- end
print('PARSE OK')
pp(genluassemblyir)
local function clean(ir)
    ir.debug = nil
    ir.source = nil
    ir.lastlinedefined = nil
    ir.linedefined = nil
    for _,p in ipairs(ir.p) do
        clean(p)
    end
end
clean(opcode54)
clean(genluassemblyir)

diffT(opcode54,genluassemblyir)
-- --
-- if not diffT(luassemblyir,genluassemblyir) then
--     pp(luassemblyir)
--     pp(genluassemblyir)
--     error('luasm didnt match')
-- end
-- print('FROM/PARSE OK')

-- local gen2opcode54 = convert.to(genluassemblyir)

-- assert(diffT(opcode54,gen2opcode54))
-- print('PARSEDTO OK')