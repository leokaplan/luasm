-- for each test in spec run each step, instead of running all steps for each spec
local inspect = require('inspect')
pp = function(...) return print(inspect(...)) end
ppi = function(...) return print(inspect(...,{newline=''})) end
pi = function(...) return inspect(...,{newline=''}) end
local luaload = load
local strip = false

local lfs = require'lfs'
local codes = {}
local function attrdir(path)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." and file ~= '_spec.lua' then
            local f = path..'/'..file
            local fun = loadfile(f)
            if fun then
                codes[file] = {}
                codes[file].bytecode = string.dump(fun,strip)
            else
                print('error lua dump',file)
            end
        end
    end
end

--attrdir ("./lua54spec") 
attrdir ("./spec") 

local function check(step,fn)
    if not next(codes, nil) then
        print('no more tests on step',step)
        return
    end
    local ok_count = 0
    local err_count = 0
    for file,code in pairs(codes) do
        local r,err =  pcall(function()fn(file,code)end) 
        if r then  
            --print('.')
            ok_count = ok_count + 1
        else
            print('error '..step,file)
            print(err)
            -- remove it so it wont propagate failure
            codes[file] = nil
            err_count = err_count + 1
        end
    end
    print('--OK/BAD  '..step,ok_count.."/"..err_count)
end
local Constants = require "5_4.constants"

local function printvals(k,v1,v2)
    if k == 'value' then
        local t1 = Constants.GetInstruction(v1)
        local t2 = Constants.GetInstruction(v2)
        print('different values',k)
        print(Constants.opnames[t1.opcode])
        ppi(t1)
        print(Constants.opnames[t2.opcode])
        ppi(t2)
    else
        print('different values',k,v1,v2)
    end
end
local function diffT(t1,t2,k)
    k = k or "root"
    local ty1 = type(t1)
    local ty2 = type(t2)
    if ty1 ~= ty2 then return false,print('different types ',k,ty1,ty2) end
    -- non-table types can be directly compared
    if ty1 == 'number' and ty2 == 'number' then return math.abs(t1-t2)<0.0000000000001 end
    if ty1 ~= 'table' and ty2 ~= 'table' then return t1 == t2 end
    -- as well as tables which have the metamethod __eq
    --local mt = getmetatable(t1)
    --if not ignore_mt and mt and mt.__eq then return t1 == t2 end
    for k1,v1 in pairs(t1) do
       local v2 = t2[k1]
       if v2 == nil or not diffT(v1,v2,k1) then return false,printvals(k1,v1,v2) end
    end
    for k2,v2 in pairs(t2) do
       local v1 = t1[k2]
       if v1 == nil or not diffT(v1,v2,k2) then return false,printvals(k2,v1,v2) end
    end
    return true
end
local function string2hex (str)
	local function tohex(c)
	  return string.format("%02x ",string.byte(c))
	end
	local hex = string.gsub(str, ".", tohex)
	return hex
end
local function split(str)
    local ret = {}
    for substring in str:gmatch("%S+") do
       table.insert(ret, substring)
    end
    return ret
end

local load = require "5_4.load"
check('load',function(file,code)
    codes[file].opcode = load.load(code.bytecode)
end)
local dump = require "5_4.dump"
check('dump',function(file,code)
    codes[file].genbytecode = dump.dump(code.opcode,strip)
end)
local instructions = {}
for k,code in pairs(codes) do
    for pc, inst in ipairs(code.opcode.code) do
        instructions[inst.opname] = true
    end
    for pk,p in ipairs(code.opcode.p) do
        for pc, inst in ipairs(p.code) do
            instructions[inst.opname] = true
        end
    end

end
for k,opname in ipairs(Constants.opnames) do
    if not instructions[opname] then print('not tested:',opname) end
end


-- print(string2hex(codes["empty.lua"].bytecode))
-- print(string2hex(codes["empty.lua"].genbytecode))
-- print(codes["empty.lua"].genbytecode==codes["empty.lua"].bytecode)
-- print(luaload(codes["empty.lua"].bytecode))
-- print(luaload(codes["empty.lua"].genbytecode))
-- check('dump loads',function(file,code)
--     --print(code.genbytecode)
--     assert(luaload(code.genbytecode))
-- end)
check('dump matches',function(file,code)
    if not (code.genbytecode == code.bytecode) then
        --diffT(code.opcode,load.load(code.genbytecode))
        --diffT(split(string2hex(code.bytecode)),split(string2hex(code.genbytecode)))
        --error('\n'..string2hex(code.bytecode)..'\n\n'..string2hex(code.genbytecode))
        error('dont match')
    end
end)
--local convert = require "5_4.convert"
--local luasm = require "src.luasm"
local luasmprint = require "src.print"
local parse = require "src.parser"
check('print',function(file,code)
    codes[file].luasm = luasmprint.print(code.opcode)
end)
check('parse',function(file,code)
    codes[file].parsed_opcode = parse.parse(code.luasm)
end)
local function clean(ir)
    ir.debug = nil
    ir.source = nil
    ir.lastlinedefined = nil
    ir.linedefined = nil
    for _,p in ipairs(ir.p) do
        clean(p)
    end
end
check('parse matches',function(file,code)
    clean(code.opcode)
    clean(code.parsed_opcode)
    if not diffT(code.opcode,code.parsed_opcode) then
        --diffT(code.opcode,load.load(code.genbytecode))
        --diffT(split(string2hex(code.bytecode)),split(string2hex(code.genbytecode)))
        --error('\n'..string2hex(code.bytecode)..'\n\n'..string2hex(code.genbytecode))
        error('parse dont match')
    end
end)
-- check('to',function(file,code)
--     codes[file].genopcode = convert.to(code.luasm_ir)
-- end)
-- check('to matches',function(file,code)
--     --TODO remove
--     convert.clean(code.opcode)
--     code.opcode.body.debug.lineinfo = {}
--     code.opcode.body.debug.locvars = {}
--     code.opcode.body.source = {}
--     assert(diffT(code.genopcode,code.opcode))
-- end)
--]]
print('PASSED:')
for k,v in pairs(codes) do print(k)end