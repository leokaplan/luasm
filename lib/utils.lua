local Utils = {}

-- deep-copy a table
function Utils.copy(obj, seen) 
    -- Handle non-tables and previously-seen tables.
    if type(obj) ~= 'table' then return obj end
    if seen and seen[obj] then return seen[obj] end

    -- New table; mark it as seen an copy recursively.
    local s = seen or {}
    local res = {}
    s[obj] = res
    for k, v in next, obj do res[Utils.copy(k, s)] = Utils.copy(v, s) end
    return setmetatable(res, getmetatable(obj))
end
-- int to bool
function Utils.toB(val)
    return val == 1 and true or false
end
-- check int
function Utils.check_int(val)
    return type(val) == 'number' and math.floor(val) == val
end

-- string to a table with its corresponding ascii
function Utils.to_ascii(str)
    local r = {}
    string.gsub(str, ".", function(i)
        table.insert(r,string.byte(i))
    end)
    return r
end

function Utils.simplecopy(x)
	local y = {}
	for k,v in ipairs(x) do
		y[k] = v
	end
	return y
end
function Utils.remove_last(x)
	local y = Utils.simplecopy(x)
	local r = y[#y]
	y[#y] = nil
	return y, r
end
function Utils.filter(table,keys)
	local ret = {}
	for k,v in ipairs(keys) do
		ret[v] = table[v]
	end
	return ret
end

function Utils.cmp_ok(a, b, labels)
	local ret = true
	for k, v in pairs(a) do
		if (v ~= b[k]) then
			if labels and labels[k] then
				print("!", labels[k], v, b[k])
			else
				print("!", k, v, b[k])
			end
			ret = false
		end
	end
	return ret
end

return Utils