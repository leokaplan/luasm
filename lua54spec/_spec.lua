--package.path = package.path..'..'
--local diff = require'diff'
local lfs = require"lfs"
local PATH_lua = '../lua-5.4.0-beta/src/lua'
local PATH_luac = '~/Documentos/lua-5.4.0-beta/src/luac'
local function attrdir (path)
    local ret = {}
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." and file ~= '_spec.lua' then
            local f = path..'/'..file
            local attr = lfs.attributes (f)
            --assert (type(attr) == "table")
            if attr.mode == "directory" then
                attrdir (f)
            else
                if string.find(file,".lua$") then
                    ret[#ret+1] = string.sub(file,1,-5)
                end
            end
        end
    end
    table.sort(ret)
    return ret
end
local files = attrdir('./examples')--attrdir ("./spec")

local load = require "5_4.load"
local dump = require "5_4.dump"
local convert = require "5_4.convert"

--os.execute("cd spec && rm *.gab && cd ..")
for _,v in pairs(files) do
    local strip = false
    local bytecode54 = string.dump(loadfile(v),strip)
    describe(v,function()
            local opcode54 = load.load(bytecode54)
            it("loads", function()assert(opcode54) end )
            local genbytecode54 = dump.dump(opcode54,strip)
            it("dumps", function()assert(genbytecode54) end)
            it('load/dump consistently',function()assert(bytecode54==genbytecode54)end)
    end)
end