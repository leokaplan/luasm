# Luasm

Luasm is an assembly-disassembly system for the Lua VM. The language it shows and reads is also called Luasm. The disassembler converts the bytecode generated from a Lua file into a source with a Luasm assembly. The assembler converts a Luasm source file into a loadable Lua bytecode.

Besides the library for converting between bytecode and Luasm, the distribution also includes a standalone program that converts Lua files into Luasm files and Luasm files into Lua bytecode files. The standalone uses the library. In particular, to convert from Lua to Luasm, the program calls the Lua compiler to emit the equivalent bytecode before disassembling it with the library.

Lua 5.4 VM is supported.

### Motivation

Currently, there is no easy way for a user to manipulate Lua VM bytecode. There is a tool that comes with the Lua distribution (luac) that only disassemble the bytecode.
That is, it does not assemble the output format back into bytecode.

The manipulation of Lua VM bytecode has a wide range of applications: One can target it when compiling a higher-level language or can perform some static analysis or optimizations.

This project aims to allow users to work at the bytecode level. It provides a human-readable format that can be produced from Lua bytecode and  assembled into a loadable bytecode. One of its main purposes is to enable the edition of the Luasm text format (manually or automatically).

### Usage

The command-line tool can transform Lua bytecode into Luasm assembly and Luasm into bytecode. 

In the distribution, the command-line tool is the executable called `luasm`. 

We will be presenting some common uses. In the following examples, we will be assuming that the commands will be run from the directory containing the file `luasm` or that it is included in your PATH.

As a first example, using the `-h` flag provides a brief self-description:

```bash
$ luasm -h
```
The command above will show the following output:
```bash
Usage: luasm [-h] [-d] [-a] <input>

A Lua vm assemble-disassemble system

Arguments:
   input                 A Lua 5.4 .lua module file or a .luasm file.

Options:
   -h, --help            Show this help message and exit.
   -d                    convert a Lua file into Luasm (default)
   -a                    assemble a Luasm file into Lua bytecode
```

To convert a Lua file to Luasm source, one can use the following command:
```bash
$ luasm -d input.lua
```
Note that the `-d` flag can be omitted. It is the default option. 

To assemble, use the `-a` flag:
```bash
$ luasm -a input.luasm > output.bytecode
```
Observe the file extension of the input. The file extension does not matter, but it is representative of its contents.


#### Converting Lua to Luasm

To assemble a Lua script, execute the following command:
```bash
$ luasm input.lua
```
Its execution will print the result in stdout.
If a file is wanted, one can redirect it to a file such as `output.luasm`
```bash
$ luasm input.lua > output.luasm
```

There is a section further below with Lua to Luasm examples and their descriptions.

#### Converting Luasm to Bytecode

The easiest way to load a bytecode assembled from Luasm is to redirect its contents to a file, which will handle the bytecode representation correctly. This file is loadable from Lua as shown below:

```
luasm -a input.luasm > output.bytecode
``` 
``` 
Lua 5.4.0  Copyright (C) 1994-2019 Lua.org, PUC-Rio
> loadfile("input.bytecode")
function: 0x55941398e4f0
``` 

Without the redirection, the `-d` flag will output a textual representation of the equivalent bytecode in stdout. This output is mostly useless since it would not even be correctly printed in a terminal and then could not be loaded. 

### Examples

We can see some examples of Lua files and their corresponding Luasm sources.

Let the content of a Lua input be the following code:
```lua
local a = 2
local b = 3
return a + b
```
Its bytecode would be converted into the following Luasm:
```
main(...):
        VARARGPREP       0
        LOADI*           R[0] 2
        LOADI            R[1] 3
        ADD              R[2] R[0] R[1]
        MMBIN            R[0] R[1] "__add"
        RETURN           R[2] 4
        RETURN           R[2] 2
```
As it is the file's body, the equivalent function is called `main`. It is variadic, as noted by the `...` in `main`'s arguments.
The generated code loads 2 registers (R[0] and R[1]) with constants, adds them into a third register (R[2]), calls the sum metamethod as a fallback if add fails and finally returns the result.

Even though the `RETURN` instruction is a ABC-type of instruction, in Luasm it has only two arguments: The first returned register and the number of registers returned after it. The second argument is calculated as described in `lopcodes.h`: A+B-2. In the first case, A = 2, B = 4 and in the second, A = 2 and B = 2. The C argument can be inferred from the context. 

For a more complex module, with function definitions, we can study the following example:
```lua
local function add(a,b)
    return a+b
end
local r = add(4,3)
print(r)
```
The assembler will output the following for this input:
```
main(...):
        VARARGPREP       0
        CLOSURE          R[0] F[0]
        MOVE             R[1] R[0]
        LOADI            R[2] 4
        LOADI            R[3] 3
        CALL             R[1] R[3] R[2]
        GETTABUP         R[2] U[0] "print"
        MOVE             R[3] R[1]
        CALL             R[2] R[2] R[1]
        RETURN           R[2] 2

main : F[0](R[0], R[1]):
        ADD              R[2] R[0] R[1]
        MMBIN            R[0] R[1] "__add"
        RETURN1          R[2]  
        RETURN0          2
```

The interesting part here is the presentation of functions.
Every file in Lua is a function, and it is possible to define functions inside of other functions. These definitions form a tree. We represent a path in this tree as `main : F[index] : F[index]` and so on. The root is always `main`.

Next we will be looking at control flow:

```lua
local a = 10
if a > 15 then
    return a
else
    while a <= 15 do
        a = a + 1
    end
end
```
it will output:

```
main(...):
        VARARGPREP       0
        LOADI            R[0] 10
        GTI              R[0] 279
        JMP              L1
        RETURN           R[0] 2
L1:     JMP              L2
        LEI              R[0] 279
        JMP              L2
        ADDI             R[0] R[0] 65658
        MMBINI           R[0] 251 "__add"
L2:     JMP              L1
        RETURN           R[1] 1

```
The Lua compiler converts control flow statements into offset-based jumps. Luasm represents these offsets as labels for easier comprehension. 

Every conditional branch is formed by a pair of instructions: a comparison and a jump. We can see similarities with arithmetic expressions, that always come accompanied by metamethods calls.




### Installation

We assume that the user has Lua 5.4 installed, with `lua5.4` present in `/usr/bin/env`.

To download the distribution of Luasm, go to https://gitlab.com/leokaplan/luasm and clone the repository.

At the root of the project, there will be the `luasm` executable. 

The library is at the `src` directory.

One can run the executable directly from its directory, but it is recommended to add it to the `PATH`.

<!-- Installation via luarocks is planned.  -->

### Testing


There are some custom test scripts in the `/tests` folder.

To run the whole suite located on `/spec`, use the following command: 

```
$ lua5.4 tests/testall.lua
```
This suite covers all opcodes. 

For each test case it does the following:
1. dumps the Lua code into bytecode
2. loads it into a table
3. dumps this value into bytecode
4. print the table as a luasm string
5. parses the luasm into a new table

Furthermore, checks if all intermediate results are consistent. An example of a consistent pair is the results from 1 and 3 being strictly the same value, without a byte of difference.  

If one wants more debugging information on a specific test, one can run:

```
$ lua5.4 tests/testprogram.lua spec/empty.lua
```

The suite mentioned above is still shallow.
On finding a test case that fails and is not covered in the suite, please report it.

In the folder lua54spec there is the test suite from  [https://www.lua.org/tests/](https://www.lua.org/tests/)
Most tests in this suite are not passing yet.


### Language reference

Here we present Luasm grammar in eBNF format. 
The top-level construct is the CHUNK.
As usual, squared-brackets-enclosed terms are optional, while enclosing a term in curly braces means zero or more repetitions of the enclosed term.

        CHUNK    ::= {FUNCTION}
        FUNCTION ::= HEADER BODY
        HEADER   ::= FUNCNAME ´(´ FUNCARGS ´):´
        FUNCNAME ::= ´main´ {´:´ ´F[´ NUMBER ´]´} 
        FUNCARGS ::= REGISTER {´,´ REGISTER} [´...´]
        BODY     ::= {COMMAND}
        COMMAND  ::= [LABEL´:´] OPNAME ARGS
        LABEL    ::= LETTER {ALPHA} 
        ALPHA    ::= LETTER | DIGIT
        LETTER   ::= "A" | "B" | "C" | "D" | "E" | "F" | "G"
                   | "H" | "I" | "J" | "K" | "L" | "M" | "N"
                   | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
                   | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
                   | "c" | "d" | "e" | "f" | "g" | "h" | "i"
                   | "j" | "k" | "l" | "m" | "n" | "o" | "p"
                   | "q" | "r" | "s" | "t" | "u" | "v" | "w"
                   | "x" | "y" | "z" 
        DIGIT    ::= "0" | "1" | "2" | "3" | "4" | "5" | "6" 
                   | "7" | "8" | "9" 
        HEXA     ::= DIGIT | "A" | "B" | "C" | "D" | "E" | "F" 
        ARGS     ::= INTEGER | FLOAT | BOOL | STRING | INDEXING
        INTEGER  ::= DIGIT {DIGIT} | ´0x´HEXA
        FLOAT    ::= DIGIT [´.´] {DIGIT}
        BOOL     ::= ´true´ | ´false´
        STRING   ::= ´'´{ALPHA}´'´|´"´{ALPHA}´"´
        INDEXING ::= LETTER ´[´ DIGIT {DIGIT} ´]´


        Notes:
                FUNCNAME indicates the tree of function declarations. All have main as root. The NUMBER indexing F denotes the prototype index and should be unique between its siblings.

                OPNAMEs are the same as the ones accepted by the Lua5.4 VM. More detailed description of ARGS are located in `luasm.lua`.

                An OPNAME can be followed by an "*", if that is the case, it means that the k flag is set. The k flag is only present in the ABC type instructions.
                 
                INDEXINGs can represent registers (R[0], R[1]...)
                or Lua internal arrays: Lua represents constants (such as strings), upvalues and function prototypes in contextual arrays (internal to the interpreter). These values can be referenced via indexings to the K, U and F arrays. 
                constants (K[0], K[1]...)
                upvalues (U[0], U[1]...)
                prototypes (F[0], F[1]...)


                Usually there is the option of using the content instead of the indexing. For example if a function has the upvalue "add" in U[0], you could use the string literal directly. Another example would be "mystring" instead of some K[3] (or 0.3 instead of an other K[2])

                Note that sometimes number literals are displayed in Luasm differently from their representation in Lua. For example, a number shown as 10 in Lua code can be internally represented as 266 and will be presented in Luasm as such. 
 
. 


#### Not Yet Implemented

- There isn't debug information such as line numbers and variable names. It is a planned feature.


#### Acknowledgments


Part of the code for assembling and disassembling Lua bytecode was done together with Gabriel Coutinho and Bruno Cuconato.