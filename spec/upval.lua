local a = 0

function b()
    local x = a
    a = 4
end
function c()
    local a = 5
    function d()
         local x = a
         a = 4
         return a
    end
    return d()
end