local a = {}
local b = 3
a[1] = 1
a["1"] = 0
a["1"] = a["1"]
a[b] = b
local c = a
function a:f() return #self end
return a[1] + a[b] + #a + a:f()
