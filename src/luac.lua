local Constants = require('constants')
local printf = function(...)
	io.write(string.format(...))
end
local function SS(x)
	return x == 1 and "" or "s"
end

local Print = {}


function Print.Header(f)
	local s = f.source and Constants.getstr(f.source) or "=?"
	local first = string.sub(s, 1, 1)
	if first == "@" or first == "=" then
		s = string.sub(s, 2)
	else
		if first == Constants.LUA_SIGNATURE[1] then
			s = "(bstring)"
		else
			s = "(string)"
		end
	end
	printf(
		"\n%s <%s:%d,%d> (%d instruction%s at ADDR)\n",
		(f.linedefined == 0) and "main" or "function",
		s,
		f.linedefined,
		f.lastlinedefined,
		f.sizecode,
		SS(f.sizecode)
	)
	printf(
		"%d%s param%s, %d slot%s, %d upvalue%s, ",
		(f.numparams),
		f.is_vararg == 1 and "+" or "",
		SS(f.numparams),
		f.maxstacksize,
		SS(f.maxstacksize),
		f.sizeupvalues,
		SS(f.sizeupvalues)
	)
	printf(
		"%d local%s, %d constant%s, %d function%s\n",
		f.sizelocvars,
		SS(f.sizelocvars),
		f.sizek,
		SS(f.sizek),
		f.sizep,
		SS(f.sizep)
	)
end

function Print.Debug(f)
	local n = f.sizek
	printf("constants (%d) for ADDR:\n", n)
	for i = 0, n - 1 do
		printf("\t%d\t", i)
		--PrintType(f, i)
		--PrintConstant(f, i)
		printf("\n")
	end
	n = f.sizelocvars
	printf("locals (%d) for ADDR:\n", n)
	for i = 0, n - 1 do
		printf("\t%d\t%s\t%d\t%d\n",
		i,
		Constants.getstr(f.locvars[i].varname),
		f.locvars[i].startpc + 1,
		f.locvars[i].endpc + 1)
	end
	n = f.sizeupvalues
	printf("upvalues (%d) for ADDR:\n", n)
	for i = 0, n - 1 do
		printf("\t%d\t%s\t%d\t%d\n", i, Constants.UPVALNAME(f, i), f.upvalues[i].instack, f.upvalues[i].idx)
	end
end
function Print.Function(f, full)
	Print.Header(f)
	Print.Code(f)
	if (full) then
		Print.Debug(f)
	end
	for _, v in pairs(f.p) do
		Print.Function(v, full)
	end
end
function Print.Code(f)
	local function MASK1(n, p)
		return ((~((0xFFFFFFFF) << (n))) << (p)) & 0xFFFFFFFF
	end
	local function PrintConstant(func, i)
		return "const " .. tostring(func) .. tostring(i)
	end

	local SIZE_C = 8
	local SIZE_B = 8
	local SIZE_Bx = (SIZE_C + SIZE_B + 1)
	local SIZE_A = 8
	local SIZE_Ax = (SIZE_Bx + SIZE_A)
	local SIZE_sJ = (SIZE_Bx + SIZE_A)

	local SIZE_OP = 7
	local POS_OP = 0

	local POS_A = (POS_OP + SIZE_OP)
	local POS_k = (POS_A + SIZE_A)
	local POS_B = (POS_k + 1)
	local POS_C = (POS_B + SIZE_B)

	local POS_Bx = POS_k
	local POS_Ax = POS_A
	local POS_sJ = POS_A

	--#if L_INTHASBITS(SIZE_sJ)
	local MAXARG_sJ = ((1 << SIZE_sJ) - 1)
	---#else
	--#define MAXARG_sJ	MAX_INT
	--#endif

	local OFFSET_sJ = (MAXARG_sJ >> 1)

	local function L_INTHASBITS(b)
		return ((Constants.UINT_MAX >> ((b) - 1)) >= 1)
	end

	local MAXARG_Bx
	if L_INTHASBITS(SIZE_Bx) then
		MAXARG_Bx = ((1 << SIZE_Bx) - 1)
	else
		MAXARG_Bx = Constants.INT_MAX
	end
	local MAXARG_C = ((1 << SIZE_C) - 1)
	local OFFSET_sBx = (MAXARG_Bx >> 1)
	local OFFSET_sC = (MAXARG_C >> 1)

	local cast_int = function(i)
		return i
	end
	local getarg = function(i, pos, size)
		return (cast_int(((i) >> (pos)) & MASK1(size, 0)))
	end
	local sC2int = function(i)
		return ((i) - OFFSET_sC)
	end
	local GET = {}
	GET.OPCODE = function(i)
		return (i >> POS_OP) & MASK1(SIZE_OP, 0)
	end
	GET.ARG_A = function(i)
		return getarg(i, POS_A, SIZE_A)
	end
	GET.ARG_B = function(i)
		return getarg(i, POS_B, SIZE_B)
	end
	GET.ARG_C = function(i)
		return getarg(i, POS_C, SIZE_C)
	end
	GET.ARG_Ax = function(i)
		return getarg(i, POS_Ax, SIZE_Ax)
	end
	GET.ARG_Bx = function(i)
		return getarg(i, POS_Bx, SIZE_Bx)
	end
	GET.ARG_sBx = function(i)
		return getarg(i, POS_Bx, SIZE_Bx) - OFFSET_sBx
	end
	GET.ARG_k = function(i)
		return getarg(i, POS_k, 1)
	end

	GET.ARG_sB = function(i)
		return sC2int(GET.ARG_B(i))
	end
	GET.ARG_sC = function(i)
		return sC2int(GET.ARG_C(i))
	end
	GET.ARG_sJ = function(i)
		-- TODO
		return getarg(i, POS_sJ, SIZE_sJ) - OFFSET_sJ
	end
	for pc, instruction_table in pairs(f.code) do
		local instruction = instruction_table.value
		local opcode = GET.OPCODE(instruction)
		local a = GET.ARG_A(instruction)
		local b = GET.ARG_B(instruction)
		local c = GET.ARG_C(instruction)
		local ax = GET.ARG_Ax(instruction)
		local bx = GET.ARG_Bx(instruction)
		local sb = GET.ARG_sB(instruction)
		local sc = GET.ARG_sC(instruction)
		local sbx = GET.ARG_sBx(instruction)
		local isk = GET.ARG_k(instruction)
		--luaG_getfuncline(f,pc);

		--print(num2hex(instruction,Instruction),'---',opnames[opcode],opcode,a,b,c,ax,bx,sb,sc,sbx,isk)

		printf("\t%d\t", pc)
		--TODO
		-- local line = pc
		-- if (line > 0) then
		-- 	printf("[%d]\t", line)
		-- else
		printf("[-]\t")
		-- end
		printf("%-9s\t", Constants.opnames[opcode])
		local COMMENT = "\t; "
		-- local VOID = function(x)
		-- 	return x
		-- end

		local eventname = function(x)
			local luaT_eventname = {
				[0] = "__index",
				"__newindex",
				"__gc",
				"__mode",
				"__len",
				"__eq",
				"__add",
				"__sub",
				"__mul",
				"__mod",
				"__pow",
				"__div",
				"__idiv",
				"__band",
				"__bor",
				"__bxor",
				"__shl",
				"__shr",
				"__unm",
				"__bnot",
				"__lt",
				"__le",
				"__concat",
				"__call",
				"__close"
			}
			return luaT_eventname[x]
		end
		if opcode == Constants.opcodes["MOVE"] then
			printf("%d %d", a, b)
		elseif opcode == Constants.opcodes["LOADI"] then
			printf("%d %d", a, sbx)
		elseif opcode == Constants.opcodes["LOADF"] then
			printf("%d %d", a, sbx)
		elseif opcode == Constants.opcodes["LOADK"] then
			printf("%d %d", a, bx)
			printf(COMMENT)
			PrintConstant(f, bx)
		elseif opcode == Constants.opcodes["LOADKX"] then
			printf("%d", a)
		elseif opcode == Constants.opcodes["LOADBOOL"] then
			printf("%d %d %d", a, b, c)
			if (c) then
				printf(COMMENT .. "to %d", pc + 2)
			end
		elseif opcode == Constants.opcodes["LOADNIL"] then
			printf("%d %d", a, b)
			printf(COMMENT .. "%d out", b + 1)
		elseif opcode == Constants.opcodes["GETUPVAL"] then
			printf("%d %d", a, b)
			printf(COMMENT .. "%s", Constants.UPVALNAME(f, b))
		elseif opcode == Constants.opcodes["SETUPVAL"] then
			printf("%d %d", a, b)
			printf(COMMENT .. "%s", Constants.UPVALNAME(f, b))
		elseif opcode == Constants.opcodes["GETTABUP"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT .. "%s", Constants.UPVALNAME(f, b))
			printf(" ")
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["GETTABLE"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["GETI"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["GETFIELD"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["SETTABUP"] then
			printf("%d %d %d%s", a, b, c, isk and "k" or "")
			printf(COMMENT .. "%s", Constants.UPVALNAME(f, a))
			printf(" ")
			PrintConstant(f, b)
			if (isk) then
				printf(" ")
				PrintConstant(f, c)
			end
		elseif opcode == Constants.opcodes["SETTABLE"] then
			printf("%d %d %d%s", a, b, c, isk and "k" or "")
			if (isk) then
				printf(COMMENT)
				PrintConstant(f, c)
			end
		elseif opcode == Constants.opcodes["SETI"] then
			printf("%d %d %d%s", a, b, c, isk and "k" or "")
			if (isk) then
				printf(COMMENT)
				PrintConstant(f, c)
			end
		elseif opcode == Constants.opcodes["SETFIELD"] then
			printf("%d %d %d%s", a, b, c, isk and "k" or "")
			printf(COMMENT)
			PrintConstant(f, b)
			if (isk) then
				printf(" ")
				PrintConstant(f, c)
			end
		elseif opcode == Constants.opcodes["NEWTABLE"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["SELF"] then
			printf("%d %d %d%s", a, b, c, isk and "k" or "")
			if (isk) then
				printf(COMMENT)
				PrintConstant(f, c)
			end
		elseif opcode == Constants.opcodes["ADDI"] then
			printf("%d %d %d %s", a, b, sc, isk and "F" or "")
		elseif opcode == Constants.opcodes["ADDK"] then
			printf("%d %d %d %s", a, b, c, isk and "F" or "")
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["SUBK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["MULK"] then
			printf("%d %d %d %s", a, b, c, isk and "F" or "")
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["MODK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["POWK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["DIVK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["IDIVK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["BANDK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["BORK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["BXORK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			PrintConstant(f, c)
		elseif opcode == Constants.opcodes["SHRI"] then
			printf("%d %d %d", a, b, sc)
		elseif opcode == Constants.opcodes["SHLI"] then
			printf("%d %d %d", a, b, sc)
		elseif opcode == Constants.opcodes["ADD"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["SUB"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["MUL"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["MOD"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["POW"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["DIV"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["IDIV"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["BAND"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["BOR"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["BXOR"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["SHL"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["SHR"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["MMBIN"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT .. "%s", eventname(c))
		elseif opcode == Constants.opcodes["MMBINI"] then
			printf("%d %d %d", a, sb, c)
			printf(COMMENT .. "%s", eventname(c))
		elseif opcode == Constants.opcodes["MMBINK"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT .. "%s ", eventname(c))
			PrintConstant(f, b)
		elseif opcode == Constants.opcodes["UNM"] then
			printf("%d %d", a, b)
		elseif opcode == Constants.opcodes["BNOT"] then
			printf("%d %d", a, b)
		elseif opcode == Constants.opcodes["NOT"] then
			printf("%d %d", a, b)
		elseif opcode == Constants.opcodes["LEN"] then
			printf("%d %d", a, b)
		elseif opcode == Constants.opcodes["CONCAT"] then
			printf("%d %d", a, b)
		elseif opcode == Constants.opcodes["CLOSE"] then
			printf("%d", a)
		elseif opcode == Constants.opcodes["TBC"] then
			printf("%d", a)
		elseif opcode == Constants.opcodes["JMP"] then
			printf("%d", GET.ARG_sJ(instruction))
			printf(COMMENT .. "to %d", GET.ARG_sJ(instruction) + pc + 2)
		elseif opcode == Constants.opcodes["EQ"] then
			printf("%d %d %d", a, b, isk)
		elseif opcode == Constants.opcodes["LT"] then
			printf("%d %d %d", a, b, isk)
		elseif opcode == Constants.opcodes["LE"] then
			printf("%d %d %d", a, b, isk)
		elseif opcode == Constants.opcodes["EQK"] then
			printf("%d %d %d", a, b, isk)
			printf(COMMENT)
			PrintConstant(f, b)
		elseif opcode == Constants.opcodes["EQI"] then
			printf("%d %d %d", a, sb, isk)
		elseif opcode == Constants.opcodes["LTI"] then
			printf("%d %d %d", a, sb, isk)
		elseif opcode == Constants.opcodes["LEI"] then
			printf("%d %d %d", a, sb, isk)
		elseif opcode == Constants.opcodes["GTI"] then
			printf("%d %d %d", a, sb, isk)
		elseif opcode == Constants.opcodes["GEI"] then
			printf("%d %d %d", a, sb, isk)
		elseif opcode == Constants.opcodes["TEST"] then
			printf("%d %d", a, isk)
		elseif opcode == Constants.opcodes["TESTSET"] then
			printf("%d %d %d", a, b, isk)
		elseif opcode == Constants.opcodes["CALL"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			if (b == 0) then
				printf("all in ")
			else
				printf("%d in ", b - 1)
			end
			if (c == 0) then
				printf("all out")
			else
				printf("%d out", c - 1)
			end
		elseif opcode == Constants.opcodes["TAILCALL"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT .. "%d in", b - 1)
		elseif opcode == Constants.opcodes["RETURN"] then
			printf("%d %d %d", a, b, c)
			printf(COMMENT)
			if (b == 0) then
				printf("all out")
			else
				printf("%d out", b - 1)
			end
		elseif opcode == Constants.opcodes["RETURN0"] then
			printf("")
		elseif opcode == Constants.opcodes["RETURN1"] then
			printf("%d", a)
		elseif opcode == Constants.opcodes["FORLOOP"] then
			printf("%d %d", a, bx)
			printf(COMMENT .. "to %d", pc - bx + 2)
		elseif opcode == Constants.opcodes["FORPREP"] then
			printf("%d %d", a, bx)
			printf(COMMENT .. "to %d", pc + bx + 2)
		elseif opcode == Constants.opcodes["TFORPREP"] then
			printf("%d %d", a, bx)
			printf(COMMENT .. "to %d", pc + bx + 2)
		elseif opcode == Constants.opcodes["TFORCALL"] then
			printf("%d %d", a, c)
		elseif opcode == Constants.opcodes["TFORLOOP"] then
			printf("%d %d", a, bx)
			printf(COMMENT .. "to %d", pc - bx + 2)
		elseif opcode == Constants.opcodes["SETLIST"] then
			printf("%d %d %d", a, b, c)
		elseif opcode == Constants.opcodes["CLOSURE"] then
			printf("%d %d", a, bx)
			printf(COMMENT .. "ADDR ")
		elseif opcode == Constants.opcodes["VARARG"] then
			printf("%d %d", a, c)
			printf(COMMENT)
			if (c == 0) then
				printf("all out")
			else
				printf("%d out", c - 1)
			end
		elseif opcode == Constants.opcodes["VARARGPREP"] then
			printf("%d", a)
		elseif opcode == Constants.opcodes["EXTRAARG"] then
			printf("%d", ax)
			printf(COMMENT)
			PrintConstant(f, ax)
		else
			error("impossible")
		end
		printf("\n")
	end
end

return Print