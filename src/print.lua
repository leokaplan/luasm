-- Prints the IR into luasm
-- it does 4 main things: 
--		printing based on semantics (printing registers as registers and not just numbers)
--		constant inlining
--		creation of labels
--		some numeric fixing
-- before it was separated in 4 different passes, but now are one

local Constants = require('5_4.constants')
local luasm = require 'src.luasm'

local Print = {}

-- prints the header
-- name (args):
function Print.Header(f,parentname)
	local ret = ""
	local name = (not f.index or f.index == 0) and "main" or parentname.." : F["..(f.index-1).."]"
	local args = ""
	for i=1,f.num_params do
		-- adds a comma if there is already an argument
		args = args == "" and args or args..", "
		-- adds the next argument
		args = args .. luasm.print_types['R'](i-1)
	end
	if f.is_vararg  == 1 then
		args = args == "" and luasm.vararg or args .. ", "..luasm.vararg
	end
	ret = ret..string.format("%s(%s):\n",name,args)
	return ret, name
end

-- currently debug information is not being dumped correctly,
-- once we fix it, we can add it in the printer.
-- We could add now, but we will lose the consistency guarantee
-- of having everything printed to be parsed and dumped correctly.


-- function Print.Debug(f)
-- 	local n = f.sizek
-- 	printf("constants (%d) for ADDR:\n", n)
-- 	for i = 0, n - 1 do
-- 		printf("\t%d\t", i)
-- 		--PrintType(f, i)
-- 		--PrintConstant(f, i)
-- 		printf("\n")
-- 	end
-- 	n = f.sizelocvars
-- 	printf("locals (%d) for ADDR:\n", n)
-- 	for i = 0, n - 1 do
-- 		printf("\t%d\t%s\t%d\t%d\n",
-- 		i, Constants.getstr(f.locvars[i].varname), f.locvars[i].startpc + 1, f.locvars[i].endpc + 1)
-- 	end
-- 	n = f.sizeupvalues
-- 	printf("upvalues (%d) for ADDR:\n", n)
-- 	for i = 0, n - 1 do
-- 		printf("\t%d\t%s\t%d\t%d\n", i, Constants.UPVALNAME(f, i), f.upvalues[i].instack, f.upvalues[i].idx)
-- 	end
-- end

-- Prints one function and its children (prototypes)
function Print.Function(f, full,parentname)
	local ret = ""
	local header, fname = Print.Header(f,parentname)
	ret = ret..header
	ret = ret..Print.Code(f)
	ret = ret..string.format("\n")
	-- if (full) then
	-- 		Print.Debug(f)
	-- end
	for _, v in pairs(f.p) do
		ret = ret..Print.Function(v, full,fname)
	end
	-- clean these or our tests will fail
	-- maybe these should be stored locally?
	f.code.labels = nil
	f.code.jumps = nil
	return ret
end

-- given a line destination, 
-- creates a label in this destination 
-- or returns the label that already exist

local function label(dest, code)
	if code[dest] then
		if not code.jumps then
			code.jumps = {}
			code.labels = 1
		end
		if code.jumps[dest] then
			return code.jumps[dest]
		end
		code.jumps[dest] = code.labels
		code[dest].label = code.labels
		code.labels = code.labels + 1
        return code.jumps[dest]
	else
		error('invalid jmp destination '..dest..' code size:'..#code)
	end
end



-- using the luasm.print table, 
-- returns the luasm representation of a given argument
-- adjust things along the way, 
-- such as number representativity, offsets and indexing from tables

local function PrintArg(arg,typ,func,dumptype,cmd,pc)
	if typ == 'I' or typ == '_' then
		return arg
	elseif typ == 'B' then
		return arg
	elseif typ == 'L' then
		return luasm.print_types['L'](label(arg+pc,func.code))
	elseif typ == 'SI' then
		return arg+ Constants.Soffset
	elseif typ == 'R' then
		return luasm.print_types['R'](arg)
	elseif typ == 'SR' then
		return luasm.print_types['R'](-(Constants.Soffset+arg))
	elseif typ == 'RK' then
		if func.k[arg+1] then
			return PrintArg(arg,'K',func,dumptype,cmd,pc)
		else
			return PrintArg(arg,'R',func,dumptype,cmd,pc)
		end
	elseif typ == 'RKS' then
		return PrintArg(arg,"RK",func,dumptype,cmd,pc)
	elseif typ == 'F' then
		return luasm.print_types['F'](arg)
	elseif typ == 'U' then
		return luasm.print_types['U'](arg)
	elseif typ == 'UV' then
		local idx = arg+Constants.Soffset+1+4 +1
		assert(func.upvalues[idx])
		return luasm.print_types['S'](Constants.getstr(func.upvalues[idx].name))
	elseif typ == 'K' then
		if type(func.k[arg+1]) == 'table' then
			return PrintArg(arg,'KS',func,dumptype,cmd,pc)
		else
			return PrintArg(arg,'KI',func,dumptype,cmd,pc)
		end
	elseif typ == 'KI' then
		local idx = arg+1
		if dumptype == 'sBx' then
			idx = idx + Constants.Soffset + 2
		end
		assert(func.k[idx])
		return tostring(func.k[idx])
	elseif typ == 'N' then
		local idx = arg+1
		assert(func.k[idx])
		return tostring(func.k[idx])
	elseif typ == 'KS' then
		local idx = arg
		--print("idx",idx,dumptype,cmd,pc)
		if dumptype == 'sBx' then
			idx = idx + Constants.Soffset + #func.k - 1023 + cmd.isk
		elseif dumptype == 'C' then
			 idx = idx + 1
		elseif dumptype == 'B' then
			 idx = idx + 1
		elseif dumptype == 'Bx' then
			idx = idx + 1 
		else
			idx = idx - cmd.isk
		end
		--print("newidx",idx)
		assert(type(func.k[idx])=='table',
			tostring(idx)..pi(func.k))
		return luasm.print_types['S'](Constants.getstr(func.k[idx]))
	elseif typ == 'MM' then
		return luasm.print_types['S'](Constants.luaT_eventname[arg])
	elseif typ == '/' then
		return ''
	else
		error(typ)
	end
end

-- sometimes, the value we need from an instruction is B
-- but it need to be loaded and dumped as Bx or sBx. 
-- This corrects this issue. Probably there is a more elegant solution.
-- if it isn't a special case, just return its value
function CorrectNumeric(cmd, dumptype)
	local value = cmd[dumptype]
	if dumptype == 'Bx' and cmd.C then
		if cmd.C ~= cmd[dumptype] - cmd.isk - 512*cmd.C then
			value = cmd[dumptype] - cmd.isk - 512*cmd.C -(cmd.C-1) -1
		else
			value = cmd.C -1
		end
	end
	if dumptype == 'sBx' and cmd.C then
		value = cmd[dumptype] - cmd.C*512
	end
	return value
end

-- prints a instruction, will print its opname 
-- and each argument in its format as specified 
-- by the opcode's high level type

local function PrintInstruction(cmd,func,pc)
	local opname = cmd.opname
	local isk = (luasm.isABC(opname) and cmd.isk == 1) and luasm.isk_symbol or ""
	local str = string.format("%-9s\t",opname..isk)
	local printtypes = luasm.types[opname][2]
	local dumptypes = luasm.types[opname][1]
	-- return is an exception
	if opname == 'RETURN' then
		local k = 1
		local valueA = CorrectNumeric(cmd,dumptypes[k])
		local RA = PrintArg(valueA,printtypes[k],func,dumptypes[k],cmd,pc)
		k = 2
		--local valueB = CorrectNumeric(cmd,dumptypes[k])
		local B = PrintArg(cmd[dumptypes[k]],printtypes[k],func,dumptypes[k],cmd,pc)
		return str.." "..RA.." "..(valueA+B-2-512)
	else
		for k,typ in ipairs(printtypes) do
			local value = CorrectNumeric(cmd,dumptypes[k])
			local arg = PrintArg(value,typ,func,dumptypes[k],cmd,pc)
			if arg ~= nil then 
				str = str .. ' '.. arg
			end
		end
	end
	return str
end


-- prints the code section of a function
-- will generate a table of strings
-- then will add the labels and concatenate everything
function Print.Code(f)
	local code = {}
	for pc, inst in ipairs(f.code) do
		code[pc] = {
			str = PrintInstruction(inst,f,pc)
		}
	end
	local ret = ""
	for pc, inst in ipairs(f.code) do
		if inst.label then
			ret = ret..string.format("%s:",luasm.print_types['L'](inst.label))
		end
		ret = ret..string.format("\t%s\n",code[pc].str)
	end
	return ret
end

-- prints an IR, starting from the main and traversing it
function Print.print(ir)
	return Print.Function(ir,"main")
end

return Print