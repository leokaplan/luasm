-- this module does 2 things: 
--      it parses a string, obtaining a syntax tree
--      it transforms the syntax tree into an IR

-- it were two different modules, 
-- but I joined them for simplicity, 
-- they were so coupled that didn't made a lot of sense
-- to have an interface

package.cpath = "lib/lpeg-1.0.2/lpeg.so;"..package.cpath
local lpeg = require "lpeg"
local pegdebug = require "lib/pegdebug"
lpeg.locale(lpeg)
local luasm = require 'src.luasm'
local Utils = require 'lib/utils'
local Constants = require('5_4.constants')


-- section 1: String to AST 

-- Lexical Elements
local Space = lpeg.S(" \n\t")^0
local SimpleSpace = lpeg.S(" \t")^0
local SimpleReqSpace = lpeg.S(" \t")^1
local NewLine = SimpleSpace * lpeg.S("\n")^1
local Open = "(" * Space
local Close = ")" * Space

-- Grammar
local Program = lpeg.V'Program'
local Function = lpeg.V'Function'
local FunctionHeader = lpeg.V'FunctionHeader'
local FunctionBody = lpeg.V'FunctionBody'
local Command = lpeg.V'Command'



local Opname = lpeg.C((lpeg.alpha+lpeg.R("09")+lpeg.P(luasm.isk_symbol))^1) -- TODO: force uppercase
local Letter = lpeg.C(lpeg.alpha)
-- maybe add support for single plicks
local String = '"' * lpeg.C( (lpeg.P(1) -'"')^0) * '"'
local Number = lpeg.Cmt(lpeg.C(lpeg.P"-"^-1 * lpeg.R("09")^1 * ( lpeg.P'.' * lpeg.R("09")^1)^0),function(_,_,c) return true,tonumber(c) end) * SimpleSpace
local NumberOrString = Number + String
-- this concept of range is deprecated
local Range = NumberOrString * ('..' * NumberOrString)^-1
local Indexing = lpeg.Ct(Letter*('['*Range*']')^1) * SimpleSpace
local FunctionName = lpeg.C('main') * (SimpleSpace *':' * SimpleSpace * Indexing)^0
local FunctionArg = lpeg.C(luasm.vararg) + Indexing
local FunctionArgs = (FunctionArg * (',' * SimpleSpace * FunctionArg)^0)^-1
local Boolean = lpeg.Cmt((lpeg.C('true')+lpeg.C('false')),function(_,_,c) return true,c=='true' end) * SimpleSpace
local SpecialArg = lpeg.C('all') * SimpleSpace
local Label = lpeg.Ct(lpeg.C('L')*Number) * SimpleSpace
local Arg =  Number + Indexing + Boolean + SpecialArg + String + Label
local Args = lpeg.Ct(((Arg * (SimpleSpace * Arg)^0))^-1)
local LabelMark = Label*lpeg.P(':')
local CommandArgs = Opname * SimpleReqSpace * Args
local CommandNoArgs = Opname * SimpleSpace
local CommandLabel = ((LabelMark * SimpleReqSpace) + SimpleSpace) * (CommandArgs + CommandNoArgs)

local grammar = { Program,
    Program = lpeg.Ct(Function^1);
    Function = lpeg.Ct(FunctionHeader * NewLine^1 * FunctionBody * Space);
    FunctionHeader = lpeg.Ct(lpeg.Ct(FunctionName) * Open * lpeg.Ct(FunctionArgs) * Close * ":");
    FunctionBody = Command ^ 0;
    Command = lpeg.Ct(CommandLabel) * NewLine;
}

local G = nil
-- for debugging
-- G = pegdebug.trace(grammar)

G = G and lpeg.P(G) or lpeg.P(grammar)
G = Space * G * -1


-- section 2: AST to IR 



local Parser = {}


-- adds the _ENV upvalue. 
-- It can be already in the stack, depending on the situation.

local function add_ENV(t,stack)
    t.upvalues[1] = {
        idx = 0,
        instack = stack and 1 or 0,
        kind = 0,
        name = { 95, 69, 78, 86 } -- _ENV
      } 
end


-- creates the structure of the IR with default values

local function create_func(main)
    local t = {
        code = {},
        is_vararg = 0,
        k = {},
        upvalues = { },
        num_params = -1,
        p = {},
        linedefined = 0,
        lastlinedefined = 0,
        sizecode = 0,
        maxstacksize = 0,
        source = {},
        debug = {
            abslineinfo = {},
            lineinfo = {},
            locvars = {},
            sizeabslineinfo = 0,
            sizelineinfo = 0,
            sizelocvars = 0
        }
    }
    if main then
        add_ENV(t,main)
    end
    return t
end


-- calculates the max stack size. 
-- for most cases, it is just the max of the registers
-- for the exceptions there are specific treatments

local function get_maxstacksize(func)
    local max_register = 0
    -- local old = 0
    for pc, cmd in pairs(func.code) do
            -- high level type
            local typ = luasm.types[cmd.opname][2]
            if cmd.opname == 'RETURN' then
                cmd.C = func.is_vararg==1 and func.num_params+1 or 0
                max_register = math.max(
                    max_register,
                    cmd.A,
                    cmd.Bx - cmd.A -512*cmd.C + 512
                )
                cmd.Bx = cmd.Bx - cmd.A+2 + 512--+512*cmd.C--+512*cmd.C+2+cmd.C
            elseif cmd.opname == 'RETURN1' then
                cmd.B = 2
            elseif cmd.opname == 'RETURN0' then
                cmd.B = 1
            elseif cmd.opname == 'CALL' then
                max_register = math.max(
                    max_register,
                    cmd.A+cmd.C-2 +1,
                    cmd.A+1 +1,
                    cmd.A+cmd.B-1 +1
                )
            elseif cmd.opname == 'TAILCALL' then
                max_register = math.max(
                    max_register,
                    cmd.A+1,
                    cmd.A+cmd.sBx+Constants.Soffset-1 +1
                )
            elseif cmd.opname == 'TFORPREP' then
                max_register = math.max(
                    max_register,
                    cmd.A+3 +1
                )
            elseif cmd.opname == 'TFORCALL' then
                max_register = math.max(
                    max_register,
                    cmd.A+4 +1,
                    cmd.A+3 +1+cmd.C+1,
                    cmd.A+1+1,
                    cmd.A+2+1
                )
            elseif cmd.opname == 'TFORLOOP' then
                max_register = math.max(
                    max_register,
                    cmd.A+2 +1
                )
            else
                cmd.sBx = (cmd.sBx and cmd.C) and (cmd.sBx+cmd.C*512) or cmd.sBx
                local maxABC = math.max(
                    typ[1]=='R' and cmd.A+1 or 0,
                    typ[2]=='R' and cmd.B and cmd.B+1 or 0,
                    typ[2]=='R' and cmd.sBx and cmd.sBx+1 or 0,
                    typ[2]=='R' and cmd.Bx and cmd.Bx+1 or 0,
                    typ[3]=='R' and cmd.C+1 or 0
                )
                max_register = math.max(max_register,maxABC)     
            end
            -- if max_register ~= old then
            --     print(cmd.opname,old,max_register)
            --     old = max_register
            -- end
    end

    return max_register
end

-- get the values that need the whole code in the IR
local function get_ir_vals(ir)
    ir.sizek = #ir.k
    ir.sizeupvalues = #ir.upvalues
    ir.sizep = #ir.p
    ir.maxstacksize = get_maxstacksize(ir)
    -- R[0] and R[1] are always available
    ir.maxstacksize = ir.maxstacksize <= 2 and 2 or ir.maxstacksize
    ir.sizecode =  #ir.code
    for _,p in ipairs(ir.p) do
        get_ir_vals(p)
    end
end

-- analogous to what happens on print, 
-- it fixes the instruction B value when it is loaded as another type
local function fix_numeric(cmd)
    local dumptypes = luasm.types[cmd.opname][1]
    
    if dumptypes[2] == 'Bx' and cmd.C then
        if cmd.C ~= cmd.Bx - cmd.isk - 512*cmd.C then
            cmd.Bx = 2*cmd.Bx + cmd.isk + 512*cmd.C --+1
        else
            cmd.Bx = cmd.C + 1
        end
    end
    
end

-- extracts the value from the AST for each high level type
-- maybe this (or parts of this) should be on luasm.lua
-- but it is so dependent of the format of the AST that it maybe 
-- dont make sense to extract 
local function get_value(value,typ,func,dumptype,inst)
    if typ == 'R' or typ == 'F' or typ == 'U' then
        assert(value and value[2],pi(value))
        return value[2]
    elseif typ == 'SR' then
        assert(value and value[2],pi(value))
        return -value[2]-Constants.Soffset
    elseif typ == 'B' then
        return value
    elseif typ == 'RK' or typ == 'RKS' then
        if type(value) == 'table' then
            return get_value(value,'R',func,dumptype,inst)
        else
            return get_value(value,'K',func,dumptype,inst)
        end
    elseif typ == 'I' then
        return value
    elseif typ == 'L' then
        assert(value and value[2],pi(value))
        return value[2]
    elseif typ == 'SI' then
        return value - Constants.Soffset
    elseif typ == 'UV' then
        local mod = dumptype == 'sBx' and Constants.Soffset+6 or 1
        local tstr = Utils.to_ascii(value)
        for k,v in ipairs(func.upvalues) do
            if value == Constants.getstr(v.name) then
                return k-mod
            end
        end
        local idx = #func.upvalues == 1 and 1 or #func.upvalues+1
        func.upvalues[idx] = {
            idx = idx-1,
            instack = 1,
            kind = 0,
            name = tstr
        }
        -- correct indexing for 0
        return #func.upvalues-mod
    elseif typ == 'KS' then
        local isk = inst.isk or 0
        local mod = dumptype == 'Bx' and 1024+1+isk or 0
        mod = 0
        local tstr = Utils.to_ascii(value)
        for k,v in ipairs(func.k) do
            if type(v) == 'table' and value == Constants.getstr(v) then
                return k-1 +mod
            end
        end
        func.k[#func.k+1] = tstr
        -- correct indexing for 0
        return #func.k-1 +mod
    elseif typ == 'K' or typ == 'KI' then
        local mod = dumptype == 'sBx' and Constants.Soffset+3 or 1
        if type(value) == "string" then
            return get_value(value,'KS',func,dumptype,inst)
        end
        for k,v in ipairs(func.k) do
            if value == v then
                return k - mod
            end
        end
        func.k[#func.k+1] = value
        -- correct indexing for 0
        return #func.k - mod
    elseif typ == 'N' then
        for k,v in ipairs(func.k) do
            if value == v then
                return k - 1
            end
        end
        func.k[#func.k+1] = value
        -- correct indexing for 0
        return #func.k-1
    elseif typ == 'MM' then
        return Constants.luaT_eventnameR[value]
    elseif typ == '_' then
        return nil
    elseif typ == '/' then
        return 0
    end
    return error(pi(typ)..','..pi(value))
end

-- transform a command from AST to ir format.
-- outputs a table with the respective low level types
local function make_inst(cmd,func)
    local inst = {}
    inst.opname = cmd[1]
    if string.sub(inst.opname, #inst.opname, #inst.opname) == luasm.isk_symbol then
        inst.opname = string.sub(inst.opname, 1, #inst.opname-1)
        inst.isk = 1
    else
        if luasm.isABC(inst.opname) then
            inst.isk = 0
        else
            --print(inst.opname)
        end
    end
    if inst.opname == 'GETTABUP' then
        --TODO maybe handle this better, 
        --for now it works
        local ismain = #func.upvalues==1
        add_ENV(func,ismain)
    end
    local args = cmd[2]
    local types = luasm.types[inst.opname]
    local lowtype = types[1]
    local hightype = types[2]
    local offset = 0
    for k,v in ipairs(lowtype) do
        inst[v] = get_value(args[k+offset],hightype[k],func,v,inst,pc)
        if hightype[k] == '_' then
            offset = offset - 1 
        end
    end
    return inst
end

-- get the values from the header and creates the function
-- updates the current pointer
local function func_header_to_ir(cmd,ir,current)
    local name = cmd[1]
    local args = cmd[2]
    
    local index
    for _,key in ipairs(name) do
        if type(key)=='table' and key[1] == 'F' then
            index = tonumber(key[2])+1
            if not current.p or type(current.p[index]) ~= 'table' then
                current.p[index] = create_func(false)
            end
            current = current.p[index]
        elseif key == 'main' then
            current = ir and ir or create_func(true)
            index = 0
        else
            error('function header')
        end
    end
    current.index = index
    current.num_params = #args
    for _,arg in ipairs(args) do
        if arg == '...' then
            current.is_vararg = 1
            if current.num_params > 0 then
                current.num_params = current.num_params - 1
            end
        end
    end
    return current
end


-- the higher level function that parses AST to IR
function Parser.to_ir(t)
    -- mount tree
    local labels = {}
    -- create the main
    local ir = create_func(true)
    -- points the current to the ir
    local current = ir
    for _,func in pairs(t) do
        for k,cmd in ipairs(func) do
            if k == 1 then -- header
                current = func_header_to_ir(cmd,ir,current)
            else
                -- case with label: LX  OPNAME ARGS
                if type(cmd[1]) == 'table' then
                    local label = table.remove(cmd,1)
                    local inst = make_inst(cmd,current)
                    fix_numeric(inst)
                    table.insert(current.code,inst)
                    labels[label[2]] = k-1 --pc
                    current.code[#current.code].label = label[2]
                
                    -- case without label: OPNAME ARGS
                else
                    local inst = make_inst(cmd,current)
                    fix_numeric(inst)
                    table.insert(current.code,inst)
                end

            end
        end
    end
    for pc, cmd in pairs(ir.code) do
        if cmd.opname == 'JMP' then
            local loc = cmd.sJ
            local offset = labels[loc]
            if not offset then 
                error('there isnt a label L'..loc)
            end
            cmd.sJ = offset-pc
            
        end
    end
    -- get values that need all the tree to be already parsed
    get_ir_vals(ir)

    ir.header = Utils.copy(Constants.header)

    -- temporary value used in the construction
    ir.index = nil
    return ir
end

-- parse a string and then convert to IR
function Parser.parse (s)
    local t = lpeg.match(G, s)
    if not t then 
        --print(s)
        lpeg.match(pegdebug.trace(grammar), s)
        error('syntax error')
    end
    return Parser.to_ir(t)
end

return Parser
