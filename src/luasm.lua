local luasm = {}

-- some constants:

-- An OPCODE followed by 
-- this symbol means it have isk set
luasm.isk_symbol = "*"

-- how we represent vararg
luasm.vararg = "..."


-- Types definitions
-- Each OPCODE have two sets of types:
--   1. the low-level  types, describing how to load and dump this value
--   2. the high-level types, describing how to print and parse and what it means, roughly

-- low types are respective to the OPCODE opmode and its possible fields,
-- such as A, B, Bx, sBx, sJ and so on. See lopcode.h and lopcode.c for reference.

-- high level types can be:
-- literals: 
--      I (integers), N (floats), SI (signed integers), B (boolean), MM (metamethods indexes), UV (the value in the upvalue)
-- registers:
--      R (register), SR (signed-value register)
-- labels:
--      L (create label to this location)
-- indexings:
--      K (constants), KS(constants but limiting to strings), KS(constants but limiting to integers), F (prototypes), U (upvalues)
-- union types:
--      RK (K[x] and K[x] or R[x]), RKS (as RK but only to strings)
-- nil type:
--      _ (this argument representation is not necessary in the luasm form) 
--      / (ignore the value during print and parse it as 0)
-- If some instruction is not properly displayed in luasm, 
-- you should assing a new high level type or create a new one, updating print.lua and parse.lua

luasm.types = {
    ["MOVE"] =        {{"A","B"  ,"C"},
                       {"R","R"  ,"_"}}
    ,["LOADI"] =      {{"A","Bx"    },
                       {"R","I"      }}
    ,["LOADF"] =      {{"A","sBx"    },
                       {"R","N"      }}
    ,["LOADK"] =      {{"A","Bx"     },
                       {"R","K"      }}
    ,["LOADKX"] =     {{"A","Bx"     },
                       {"R","_"      }}
    ,["LOADBOOL"] =   {{"A","B"  ,"C"},
                       {"R","B"  ,"B"}}
    ,["LOADNIL"] =    {{"A","B"  ,"C"},
                       {"I","I"  ,"_"}}--  transform into ranGe (old RR)
    ,["GETUPVAL"] =   {{"A","sBx","C"},
                       {"R","UV","I" }}
    ,["SETUPVAL"] =   {{"A","B","C"  },
                       {"R","U","_"  }} -- maybe invert
    ,["GETTABUP"] =   {{"A","B","C"  },
                       {"R","U","KS" }}
    ,["GETTABLE"] =   {{"A","B","C"  },
                       {"R","R","R"  }}
    ,["GETI"] =       {{"A","B","C"  },
                       {"R","R","I"  }}
    ,["GETFIELD"] =   {{"A","B","C"  },
                       {"R","R","KS" }}
    ,["SETTABUP"] =   {{"A","B","C"  },
                       {"U","KS","RK"}}
    ,["SETTABLE"] =   {{"A","B","C"  },
                       {"R","R","RK" }}
    ,["SETI"] =       {{"A","sBx","C"},
                       {"R","I","RK" }}
    ,["SETFIELD"] =   {{"A","Bx","C"},
                       {"R","KS","RK"}}
    ,["NEWTABLE"] =   {{"A","B","C"  },
                       {"R","I","I"  }}
    ,["SELF"] =       {{"A","sBx","C"},
                       {"R","SR","RKS"}}
    ,["ADDI"] =       {{"A","B","C"  },
                       {"R","R","SI" }} -- sI = signed int, "dump sC" in this case
    ,["ADDK"] =       {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["SUBK"] =       {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["MULK"] =       {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["MODK"] =       {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["POWK"] =       {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["DIVK"] =       {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["IDIVK"] =      {{"A","B","C"  },
                       {"R","R","K"  }}
    ,["BANDK"] =      {{"A","B","C"  },
                       {"R","R","KI" }}
    ,["BORK"] =       {{"A","B","C"  },
                       {"R","R","KI" }}
    ,["BXORK"] =      {{"A","B","C"  },
                       {"R","R","KI" }}
    ,["SHRI"] =       {{"A","B","C"},
                       {"R","R","SI"}}
    ,["SHLI"] =       {{"A","B","C"},
                       {"R","R","SI"}}
    ,["ADD"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["SUB"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["MUL"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["MOD"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["POW"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["DIV"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["IDIV"] =       {{"A","B","C"},
                       {"R","R","R"}}
    ,["BAND"] =       {{"A","B","C"},
                       {"R","R","R"}}
    ,["BOR"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["BXOR"] =       {{"A","B","C"},
                       {"R","R","R"}}
    ,["SHL"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["SHR"] =        {{"A","B","C"},
                       {"R","R","R"}}
    ,["MMBIN"] =      {{"A","B","C"},
                       {"R","R","MM"}}
    ,["MMBINI"] =     {{"A","sBx","C"},
                       {"R","SI","MM"}}
    ,["MMBINK"] =     {{"A","B","C"},
                       {"R","K","MM"}}
    ,["UNM"] =        {{"A","B","C"},
                       {"R","R","_"}}
    ,["BNOT"] =       {{"A","B","C"},
                       {"R","R","_"}}
    ,["NOT"] =        {{"A","B","C"},
                       {"R","R","_"}}
    ,["LEN"] =        {{"A","B","C"},
                       {"R","R","_"}}
    ,["CONCAT"] =     {{"A","B","C"},
                       {"R","R","_"}}
    ,["CLOSE"] =      {{"A","B","C"},
                       {"R","_","_"}}
    ,["TBC"] =        {{"A","B","C"},
                       {"I","I","I"}}
    ,["JMP"] =        {{"sJ"},
                       {"L"}} 
    ,["EQ"] =         {{"A","sBx","C"},
                       {"R","R","_"}}
    ,["LT"] =         {{"A","sBx","C"},
                       {"R","R","_"}}
    ,["LE"] =         {{"A","sBx","C"},
                       {"R","R","_"}}
    ,["EQK"] =        {{"A","sBx","C"},
                       {"R","K","_"}}
    ,["EQI"] =        {{"A","sBx","C"},
                       {"R","SI","_"}}
    ,["LTI"] =        {{"A","sBx","C"},
                       {"R","SI","_"}}
    ,["LEI"] =        {{"A","sBx","C"},
                       {"R","SI","_"}}
    ,["GTI"] =        {{"A","sBx","C"},
                       {"R","SI","_"}}
    ,["GEI"] =        {{"A","sBx","C"},
                       {"R","SI","_"}}
    ,["TEST"] =       {{"A","B","C"},
                       {"R","_","_"}}
    ,["TESTSET"] =    {{"A","sBx","C"},
                       {"R","R","_"}}
    ,["CALL"] =       {{"A","B","C"},
                       {"R","R","R"}}
    ,["TAILCALL"] =   {{"A","sBx","C"},
                       {"R","SR","_"}}
    ,["RETURN"] =     {{"A","Bx","C"},
                       {"R","I", "I"}}
    ,["RETURN0"] =    {{"A","B","C"},
                       {"I","/","/"}}
    ,["RETURN1"] =    {{"A","B","C"},
                       {"R","/","/"}}
    ,["FORLOOP"] =    {{"A","Bx"},
                       {"I","I"}}
    ,["FORPREP"] =    {{"A","sBx"},
                       {"I","SI"}}
    ,["TFORPREP"] =   {{"A","Bx"},
                       {"R","I"}}
    ,["TFORCALL"] =   {{"A","B","C"},
                       {"R","_","R"}}
    ,["TFORLOOP"] =   {{"A","Bx"},
                       {"R","I"}}
    ,["SETLIST"] =    {{"A","B","C"},
                       {"R","I","I"}}
    ,["CLOSURE"] =    {{"A","Bx"},
                       {"R","F"}}
    ,["VARARG"] =     {{"A","B","C"},
                       {"R","_","R"}}
    ,["VARARGPREP"] = {{"A","B","C"},
                       {"I","_","_"}}
    ,["EXTRAARG"] =   {{"Ax"},
                       {"I"}}
}
function luasm.isABC(opname) 
	local dump = luasm.types[opname][1]
	return 
		dump[1] == 'A' and 
		--dump[2] == 'B' and
		dump[3] == 'C'
end

luasm.print_types = {
    ['R'] = function(arg) return 'R['..arg..']' end,
    ['F'] = function(arg) return 'F['..arg..']' end,
    ['U'] = function(arg) return 'U['..arg..']' end,
    ['S'] = function(arg) return '"'..arg..'"' end,
    ['L'] = function(arg) return 'L'..arg end
}

return luasm