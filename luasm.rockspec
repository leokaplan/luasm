package = "luasm"
version = "0.1"
source = {
   url = "git+https://gitlab.com/leokaplan/luasm.git"
}
description = {
   summary = "An assembly language for Lua",
   detailed = "",
   homepage = "https://gitlab.com/leokaplan/luasm",
   license = "MIT"
}
dependencies = {
   "lua == 5.4",
   "argparse",
   "busted",
   "lfs"
}
build = {
   type = "builtin"
}